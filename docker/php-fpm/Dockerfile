FROM alpine:3.12

ENV gid=1000 uid=1000

RUN apk add bash

RUN apk add --no-cache sudo

RUN apk add --no-cache git

RUN apk add --update --no-cache \
    coreutils \
    php7-fpm \
    php7-apcu \
    php7-ctype \
    php7-curl \
    php7-dom \
    php7-gd \
    php7-iconv \
    php7-imagick \
    php7-json \
    php7-intl \
    php7-mcrypt \
    php7-fileinfo\
    php7-mbstring \
    php7-opcache \
    php7-openssl \
    php7-pdo \
    php7-pdo_mysql \
    php7-mysqli \
    php7-xml \
    php7-zlib \
    php7-phar \
    php7-tokenizer \
    php7-session \
    php7-simplexml \
    php7-pecl-xdebug \
    php7-zip \
    php7-xmlwriter \
    php7-xmlreader \
    make \
    curl \
    yarn

RUN apk add --update --no-cache \
    php7-pdo_sqlite

RUN curl -sS https://getcomposer.org/installer | php -- \
--install-dir=/usr/bin --filename=composer && chmod +x /usr/bin/composer 

COPY symfony.ini /etc/php7/conf.d/
COPY symfony.ini /etc/php7/cli/conf.d/
COPY xdebug.ini  /etc/php7/conf.d/

COPY symfony.pool.conf /etc/php7/php-fpm.d/

CMD ["php-fpm7", "-F"]

WORKDIR /var/www

RUN adduser -u $uid -D developer

RUN chmod g+s /var/log && \
    touch /var/log/php7/error.log && \
    chmod 775 /var/log/php7 && \
    chmod 755 /var/log/php7/error.log && \
    chown -R developer:developer /var/log

USER developer

EXPOSE 9001
